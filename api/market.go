package api

import (
	"net/http"
	"unico/internal/unico/models"
	"unico/internal/unico/services"
	"unico/pkg/rest"

	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
)

// GetMarket godoc
// @Summary Get existing market
// @Description Get all the existing market
// @Accept  json
// @Produce  json
// @Param  id path string true "Market ID"
// @Success 200  {object} entities.Market
// @Failure 404 {object} rest.Response
// @Failure 400 {object} rest.Response
// @Failure 500 {object} rest.Response
// @Router /market/:id [get]
func (ch *controlerHandler) GetMarket(c *gin.Context) {
	id, err := uuid.FromString(c.Param("id"))
	if err != nil {
		rest.ResponseBadRequest(c, err)
		return
	}
	market, err := ch.db.GetMarket(id)
	if err != nil {
		log.Error(err)
		rest.ResponseBadRequest(c, err)
		return
	}
	if market == nil {
		c.JSON(http.StatusNotFound, gin.H{})
		return
	}
	c.JSON(http.StatusOK, market)
}

// GetAllMarket godoc
// @Summary List existing market
// @Description Get all the existing market
// @Accept  json
// @Produce  json
// @Success 200 {array} entities.Market
// @Failure 500 {object} rest.Response
// @Router /market [get]
func (ch *controlerHandler) GetAllMarket(c *gin.Context) {
	markets, err := ch.db.GetAllMarket()
	if err != nil {
		log.Error(err)
		rest.ResponseBadRequest(c, err)
		return
	}
	c.JSON(http.StatusOK, markets)
}

// CreateMarket godoc
// @Summary Create Market
// @Description Create a new Market
// @Accept  json
// @Produce  json
// @Param market body models.MarketCreateRequest true "Create Market"
// @Success 200
// @Failure 500 {object} rest.Response
// @Router /market [post]
func (ch *controlerHandler) CreateMarket(c *gin.Context) {
	marketreq := new(models.MarketCreateRequest)
	err := c.BindJSON(&marketreq)
	if err != nil {
		rest.ResponseBadRequest(c, err)
		return
	}
	// Validade Request
	err = ch.validate.Struct(marketreq)
	if err != nil {
		rest.ResponseBadRequest(c, err)
		return
	}

	// Create New Market
	err = services.CreateMarket(marketreq, ch.db)
	if err != nil {
		rest.ResponseBadRequest(c, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

// UpdateMarket godoc
// @Summary Create Market
// @Description Create a new Market
// @Accept  json
// @Produce  json
// @Param market body models.MarketUpdateRequest true "Update Market"
// @Success 200
// @Failure 500 {object} rest.Response
// @Router /market [put]
func (ch *controlerHandler) UpdateMarket(c *gin.Context) {
	marketreq := new(models.MarketCreateRequest)
	err := c.BindJSON(&marketreq)
	if err != nil {
		rest.ResponseBadRequest(c, err)
		return
	}
	// Validade Request
	err = ch.validate.Struct(marketreq)
	if err != nil {
		rest.ResponseBadRequest(c, err)
		return
	}

}

// RemoveMarket godoc
// @Summary Remove Market
// @Description Remove Market by ID
// @Accept  json
// @Produce  json
// @Success 200
// @Failure 500 {object} rest.Response
// @Router /market/{id} [delete]
func (ch *controlerHandler) RemoveMarket(c *gin.Context) {
	id, err := uuid.FromString(c.Param("id"))
	if err != nil {
		rest.ResponseBadRequest(c, err)
		return
	}

	market, err := ch.db.GetMarket(id)
	if err != nil {
		log.Error(err)
		rest.ResponseBadRequest(c, err)
		return
	}
	if market == nil {
		c.JSON(http.StatusNotFound, gin.H{})
		return
	}

	// Remove Record
	err = ch.db.RemoveMarket(id)
	if err != nil {
		log.Error(err)
		rest.ResponseBadRequest(c, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}
