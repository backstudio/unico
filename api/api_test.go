package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"unico/internal/unico/database"
	"unico/internal/unico/entities"
	"unico/internal/unico/migrations"
	"unico/internal/unico/models"
	"unico/internal/unico/services"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var server *gin.Engine
var db *database.DatabaseHandle

func init() {
	server = gin.Default()
	connectiondb, err := gorm.Open(postgres.New(postgres.Config{
		DSN:                  os.Getenv("DB_CONNECTION"),
		PreferSimpleProtocol: true,
	}), &gorm.Config{})

	if err != nil {
		log.Panic("Failed to connect to database: ", err)
	}

	err = migrations.Run(connectiondb)
	if err != nil {
		log.Panic("Failed to run migration: ", err)
	}

	db = database.NewDatabaseHandle(connectiondb)
	ConfigureRoutes(server, db)

}
func TestNpRoute(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/norount", nil)
	server.ServeHTTP(w, req)
	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestHttpCreateAccount(t *testing.T) {
	w := httptest.NewRecorder()
	jsonValue, _ := json.Marshal(gin.H{"document_number": gofakeit.DigitN(10)})
	req, err := http.NewRequest("POST", "/api/v1/account", bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json") // This makes it work
	if err != nil {
		log.Println(err)
	}
	server.ServeHTTP(w, req)
	assert.Equal(t, http.StatusCreated, w.Code)
}
func TestHttpErrorCreateAccount(t *testing.T) {
	w := httptest.NewRecorder()
	jsonValue, _ := json.Marshal(gin.H{"document_number": ""})
	req, err := http.NewRequest("POST", "/api/v1/account", bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json") // This makes it work
	if err != nil {
		log.Println(err)
	}
	server.ServeHTTP(w, req)
	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestHttpGetAccount(t *testing.T) {

	document := gofakeit.DigitN(10)

	// Create Account
	request := &models.AccountCreateRequest{
		DocumentNumber: document,
	}
	account, err := services.CreateAccount(request, db)
	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", fmt.Sprintf("/api/v1/account/%d", account.AccountID), nil)
	req.Header.Set("Content-Type", "application/json") // This makes it work
	if err != nil {
		log.Println(err)
	}
	server.ServeHTTP(w, req)

	// Check Resposnse Status
	assert.Equal(t, http.StatusOK, w.Code)

	// Check Response Data
	accountResponse := &entities.Account{}
	json.NewDecoder(w.Body).Decode(&accountResponse)
	assert.Equal(t, accountResponse.DocumentNumber, document)
}

func TestHttpTransaction(t *testing.T) {
	w := httptest.NewRecorder()

	accounts := []struct {
		document  string
		operation entities.OperationType
		amount    float64
	}{
		{gofakeit.DigitN(10), entities.OperationTypePayment, 200.45},
		{gofakeit.DigitN(10), entities.OperationTypeWithdraw, 73600.07},
		{gofakeit.DigitN(10), entities.OperationTypeInstallment, 345.60},
		{gofakeit.DigitN(10), entities.OperationTypeCashPayment, 43800.45},
	}

	for _, acc := range accounts {

		// Create Account
		request := &models.AccountCreateRequest{
			DocumentNumber: acc.document,
		}
		account, err := services.CreateAccount(request, db)
		if err != nil {
			t.Fatal(err)
		}

		jsonValue, _ := json.Marshal(gin.H{
			"operation_type_id": acc.operation,
			"account_id":        account.AccountID,
			"amount":            acc.amount,
		})

		req, err := http.NewRequest("POST", "/api/v1/transactions", bytes.NewBuffer(jsonValue))
		req.Header.Set("Content-Type", "application/json") // This makes it work
		if err != nil {
			log.Println(err)
		}
		server.ServeHTTP(w, req)
		assert.Equal(t, http.StatusCreated, w.Code)
	}
}

func TestHttpENotFoundTransaction(t *testing.T) {
	w := httptest.NewRecorder()
	jsonValue, _ := json.Marshal(gin.H{
		"operation_type_id": 1,
		"account_id":        48458,
		"amount":            25.525,
	})

	req, err := http.NewRequest("POST", "/api/v1/transactions", bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json") // This makes it work
	if err != nil {
		log.Println(err)
	}
	server.ServeHTTP(w, req)
	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestHttpErrorTransaction(t *testing.T) {
	w := httptest.NewRecorder()

	accounts := []struct {
		document  string
		operation entities.OperationType
		amount    float64
	}{
		{gofakeit.DigitN(10), entities.OperationTypePayment, 200.45},
		{gofakeit.DigitN(10), entities.OperationTypeWithdraw, 73600.07},
		{gofakeit.DigitN(10), entities.OperationTypeInstallment, 345.60},
		{gofakeit.DigitN(10), entities.OperationTypeCashPayment, 43800.45},
	}

	for _, acc := range accounts {

		// Create Account
		request := &models.AccountCreateRequest{
			DocumentNumber: acc.document,
		}
		account, err := services.CreateAccount(request, db)
		if err != nil {
			t.Fatal(err)
		}

		jsonValue, _ := json.Marshal(gin.H{
			"operation_type_id": acc.operation,
			"account_id":        account.AccountID,
		})

		req, err := http.NewRequest("POST", "/api/v1/transactions", bytes.NewBuffer(jsonValue))
		req.Header.Set("Content-Type", "application/json") // This makes it work
		if err != nil {
			log.Println(err)
		}
		server.ServeHTTP(w, req)
		assert.Equal(t, http.StatusBadRequest, w.Code)
	}
}
