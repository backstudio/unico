package services

import (
	"context"
	"encoding/csv"
	"mime/multipart"
	"strconv"
	"unico/internal/unico/database"
	"unico/internal/unico/entities"
)

func ImportDataCSV(ctx context.Context, db database.Database, file *multipart.FileHeader) error {
	filetemp, err := file.Open()

	reader := csv.NewReader(filetemp)

	reader.FieldsPerRecord = -1
	record, err := reader.ReadAll()

	if err != nil {
		log.Errorf("Error to read file: %s", err)
		return err
	}

	for _, line := range record[1:] {

		// Save subPrefecture
		id, _ := strconv.ParseUint(line[7], 10, 32)

		subPrefecture := &entities.SubPrefecture{}
		subPrefecture.ID = uint(id)
		subPrefecture.Name = line[8]
		subPrefecture.Zone = entities.Zone(line[9])

		err := db.CreateSubPrefecture(subPrefecture)
		if err != nil && err != database.ErrAlreadyExist {
			return err
		}

		// Save District
		id, _ = strconv.ParseUint(line[5], 10, 32)
		district := &entities.District{}
		district.ID = uint(id)
		district.Name = line[6]
		district.SubPrefectureID = subPrefecture.ID

		err = db.CreateDistrict(district)
		if err != nil && err != database.ErrAlreadyExist {
			return err
		}

		// Save Market
		market := &entities.Market{}
		market.Name = line[11]
		market.Latitude = line[1]
		market.Longitude = line[2]
		market.SetCents = line[3]
		market.Area = line[4]

		market.Zone8 = line[10]

		market.Record = line[12]
		market.Street = line[13]
		market.Number = line[14]
		market.Neighborhood = line[15]

		if len(line) == 17 {
			market.Reference = line[16]
		}

		market.DistrictID = district.ID
		market.SubPrefectureID = subPrefecture.ID

		err = db.CreateMarket(market)
		if err != nil {
			return err
		}

	}
	return nil
}
