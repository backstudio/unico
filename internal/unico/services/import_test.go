package services

import (
	"log"
	"os"
	"testing"
	"unico/internal/unico/database"
	"unico/internal/unico/entities"
	"unico/internal/unico/migrations"
	"unico/internal/unico/models"
	"unico/pkg/database/model"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/go-playground/assert/v2"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *database.DatabaseHandle

func init() {
	gofakeit.Seed(0)
	connectiondb, err := gorm.Open(postgres.New(postgres.Config{
		DSN:                  os.Getenv("DB_CONNECTION"),
		PreferSimpleProtocol: true,
	}), &gorm.Config{})

	if err != nil {
		log.Panic("Failed to connect to database: ", err)
	}

	err = migrations.Run(connectiondb)
	if err != nil {
		log.Panic("Failed to run migration: ", err)
	}

	db = database.NewDatabaseHandle(connectiondb)
}

func TestCreateAccount(t *testing.T) {
	document := gofakeit.DigitN(10)
	request := &models.AccountCreateRequest{
		DocumentNumber: document,
	}
	account, err := CreateAccount(request, db)
	if err != nil {
		t.Fatal(err)
	}

	if account.DocumentNumber != document {
		t.Fatal("Erro Create Account")
	}

	if account.AvailableCreditLimit != model.Money(500) {
		t.Fatal("Erro Create Account")
	}

	if account.CreditLimit != model.Money(500) {
		t.Fatal("Erro Create Account")
	}
}

func TestGetAccount(t *testing.T) {
	document := gofakeit.DigitN(10)
	request := &models.AccountCreateRequest{
		DocumentNumber: document,
	}
	account, err := CreateAccount(request, db)
	if err != nil {
		t.Fatal(err)
	}
	accountget, err := GetAccount(account.AccountID, db)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, accountget.AccountID, account.AccountID)
}

func TestTransaction(t *testing.T) {

	accounts := []struct {
		document     string
		operation    entities.OperationType
		amount       float64
		valueCompare float64
	}{
		{gofakeit.DigitN(10), entities.OperationTypePayment, 100.30, 500 + 100.30},
		{gofakeit.DigitN(10), entities.OperationTypeWithdraw, 36.60, 500 - 36.60},
		{gofakeit.DigitN(10), entities.OperationTypeInstallment, 10.00, 500 - 10.00},
		{gofakeit.DigitN(10), entities.OperationTypeCashPayment, 250.58, 500 - 250.58},
	}
	for _, acc := range accounts {
		accountrequest := &models.AccountCreateRequest{
			DocumentNumber: acc.document,
		}
		account, err := CreateAccount(accountrequest, db)
		if err != nil {
			t.Fatal(err)
		}
		request := &models.TransactionCreateRequest{
			AccountID:       account.AccountID,
			Amount:          acc.amount,
			OperationTypeID: acc.operation,
		}
		err = CreateTransaction(request, account, db)
		if err != nil {
			t.Fatal(err)
		}

		account, err = db.GetAccount(account.AccountID)
		if err != nil {
			t.Fatal(err)
		}

		assert.Equal(t, account.AvailableCreditLimit, model.Money(acc.valueCompare))
	}
}

func TestTransactionNotAvailable(t *testing.T) {

	accounts := []struct {
		document     string
		operation    entities.OperationType
		amount       float64
		valueCompare float64
	}{
		{gofakeit.DigitN(10), entities.OperationTypeWithdraw, 600.60, 500 - 100.60},
		{gofakeit.DigitN(10), entities.OperationTypeInstallment, 501.00, 500 - 501.00},
		{gofakeit.DigitN(10), entities.OperationTypeCashPayment, 780.58, 500 - 501.58},
	}
	for _, acc := range accounts {
		accountrequest := &models.AccountCreateRequest{
			DocumentNumber: acc.document,
		}
		account, err := CreateAccount(accountrequest, db)
		if err != nil {
			t.Fatal(err)
		}
		request := &models.TransactionCreateRequest{
			AccountID:       account.AccountID,
			Amount:          acc.amount,
			OperationTypeID: acc.operation,
		}
		err = CreateTransaction(request, account, db)
		if err == nil {
			t.Fatal(err)
		}
	}
}
