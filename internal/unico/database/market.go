package database

import (
	"errors"
	"unico/internal/unico/entities"
	logger "unico/pkg/log"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

var ErrAlreadyExist = errors.New("Record Already Exist")

var log *logger.Logger

func init() {
	log = logger.NewLogger()
}

type (
	DatabaseHandle struct {
		db *gorm.DB
	}
)

func NewDatabaseHandle(db *gorm.DB) *DatabaseHandle {
	return &DatabaseHandle{db: db}
}

func (r *DatabaseHandle) GetDB() *gorm.DB {
	return r.db
}

// GetMarket
func (r *DatabaseHandle) GetMarket(id uuid.UUID) (*entities.Market, error) {
	market := &entities.Market{}
	err := r.db.Model(&entities.Market{}).Preload("SubPrefecture").Preload("District").Where("id = ?", id).First(market).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return market, nil
}

// GetAllMarket
func (r *DatabaseHandle) GetAllMarket() ([]entities.Market, error) {
	var markets = []entities.Market{}
	err := r.db.Model(&entities.Market{}).Preload("SubPrefecture").Preload("District").Find(&markets).Error
	return markets, err
}

// CreateMarket
func (r *DatabaseHandle) CreateMarket(market *entities.Market) error {
	return r.db.Create(market).Error
}

// RemoveMarket
func (r *DatabaseHandle) RemoveMarket(id uuid.UUID) error {
	return r.db.Delete(&entities.Market{}, id).Error
}
