package database

import (
	"unico/internal/unico/entities"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

type (
	Database interface {
		GetDB() *gorm.DB
		CreateMarket(market *entities.Market) error
		GetMarket(id uuid.UUID) (*entities.Market, error)
		RemoveMarket(id uuid.UUID) error
		GetAllMarket() ([]entities.Market, error)
		CreateSubPrefecture(subPrefecture *entities.SubPrefecture) error
		CreateDistrict(district *entities.District) error
		GetAllDistrict(subprefecture uint) ([]entities.District, error)
		GetAllSubPrefecture() ([]entities.SubPrefecture, error)
	}
)
