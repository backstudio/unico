package models

type MarketCreateRequest struct {
	Name            string `json:"name"`
	Record          string `json:"record"`
	Latitude        int    `json:"latitude"`
	Longitude       int    `json:"longitude"`
	SetCents        string `json:"setcents" `
	SubPrefectureID uint   `json:"subPrefectureID,omitempty"`
	DistrictID      uint   `json:"districtID,omitempty"`
	Zone8           string `json:"zone8,omitempty"`
	Street          string `json:"street"`
	Number          string `json:"number"`
	Neighborhood    string `json:"neighborhood"`
	Reference       string `json:"reference"`
}

type MarketUpdateRequest struct {
	Name            string `json:"name"`
	Latitude        int    `json:"latitude"`
	Longitude       int    `json:"longitude"`
	SetCents        string `json:"setcents" `
	SubPrefectureID uint   `json:"subPrefectureID,omitempty"`
	DistrictID      uint   `json:"districtID,omitempty"`
	Zone8           string `json:"zone8,omitempty"`
	Street          string `json:"street"`
	Number          string `json:"number"`
	Neighborhood    string `json:"neighborhood"`
	Reference       string `json:"reference"`
}
